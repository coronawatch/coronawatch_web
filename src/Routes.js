import React,{useContext} from 'react';
import { Switch, Redirect } from 'react-router-dom';
import { AuthContext , AuthProvider} from './components/Auth';
import { RouteWithLayout } from './components';
import { Main as MainLayout, Minimal as MinimalLayout } from './layouts';

import {
  Dashboard as DashboardView,
  ProductList as ProductListView,
  UserList as UserListView,
  Typography as TypographyView,
  Icons as IconsView,
  Account as AccountView,
  Settings as SettingsView,
  SignUp as SignUpView,
  SignIn as SignInView,
  NotFound as NotFoundView,
  Invalide as InvalideView,
  VideoInt as VideoIntView,
  CasSuspect as CasSuspectView,
  Zone as ZoneView
} from './views';

const Routes =  ({ component: RouteComponent, ...rest }) => {
  const {currentUser} = useContext(AuthContext);
  return (
    <RouteWithLayout>
      <AuthContext.Consumer>
      {value=>{
          return(
            !!currentUser ? (
    <Switch>
      <Redirect
        exact
        from="/"
        to="/dashboard"
      />
      <RouteWithLayout
        component={DashboardView}
        exact
        layout={MainLayout}
        path="/dashboard"
      />
      <RouteWithLayout
        component={UserListView}
        exact
        layout={MainLayout}
        path="/users"
      />
        <RouteWithLayout
        component={InvalideView}
        exact
        layout={MainLayout}
        path="/invalide"
      />
      <RouteWithLayout
        component={ProductListView}
        exact
        layout={MainLayout}
        path="/products"
      />
      <RouteWithLayout
        component={TypographyView}
        exact
        layout={MainLayout}
        path="/typography"
      />
      <RouteWithLayout
        component={IconsView}
        exact
        layout={MainLayout}
        path="/icons"
      />
      <RouteWithLayout
        component={AccountView}
        exact
        layout={MainLayout}
        path="/account"
      />
      <RouteWithLayout
        component={SettingsView}
        exact
        layout={MainLayout}
        path="/settings"
      />
      <RouteWithLayout
        component={SignUpView}
        exact
        layout={MinimalLayout}
        path="/sign-up"
      />
      <RouteWithLayout
        component={SignInView}
        exact
        layout={MinimalLayout}
        path="/sign-in"
      />
      <RouteWithLayout
        component={NotFoundView}
        exact
        layout={MinimalLayout}
        path="/not-found"
      />
       <RouteWithLayout
        component={CasSuspectView}
        exact
        layout={MainLayout}
        path="/CasSuspect"
      />
         <RouteWithLayout
        component={ZoneView}
        exact
        layout={MainLayout}
        path="/zone"
      />
           <RouteWithLayout
        component={VideoIntView}
        exact
        layout={MainLayout}
        path="/videoInt"
      />
      <Redirect to="/not-found" />
    </Switch>
     ) : (
                 
      <Switch>
     
    
      <RouteWithLayout
        component={SignInView}
        exact
        layout={MinimalLayout}
        path="/sign-in"
      
      />
      <Redirect to="/sign-in" />
    </Switch>
                )
            )
        }}
        </AuthContext.Consumer>
      </RouteWithLayout>
       
      
  );
};

export default Routes;
