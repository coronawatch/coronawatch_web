
import React from 'react';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';
import clsx from 'clsx';
import LocalHospitalIcon from '@material-ui/icons/LocalHospital';
import MovieIcon from '@material-ui/icons/Movie';
import LanguageIcon from '@material-ui/icons/Language';
import FacebookIcon from '@material-ui/icons/Facebook';
import SettingsIcon from '@material-ui/icons/Settings';
import RoomIcon from '@material-ui/icons/Room';
import YouTubeIcon from '@material-ui/icons/YouTube';
import VideocamIcon from '@material-ui/icons/Videocam';
import VideoCallIcon from '@material-ui/icons/VideoCall';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import PublicIcon from '@material-ui/icons/Public';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import { Divider, Drawer } from '@material-ui/core';
import DashboardIcon from '@material-ui/icons/Description';
import PeopleIcon from '@material-ui/icons/ReportProblemOutlined';
import ShoppingBasketIcon from '@material-ui/icons/LanguageOutlined';
import TextFieldsIcon from '@material-ui/icons/PublicOutlined';
import ImageIcon from '@material-ui/icons/EqualizerOutlined';
//import AccountBoxIcon from '@material-ui/icons/AccountBox';
import Button from '@material-ui/core/Button';

//import SettingsIcon from '@material-ui/icons/Settings';
import LockOpenIcon from '@material-ui/icons/VideocamOutlined';

import { Profile, SidebarNav, UpgradePlan } from './components';

const useStyles = makeStyles(theme => ({
  drawer: {
    width: 240,
    [theme.breakpoints.up('lg')]: {
    
      height: '100%'
    }
  },
  root: {
    backgroundColor: '#4EACAC',
    display: 'flex',
    flexDirection: 'column',
    height: '200%',
    padding: theme.spacing(2)
  },
  divider: {
    margin: theme.spacing(2, 0)
  },
  nav: {
   // marginBottom: theme.spacing(2)
  },
  Button:{
    color:"#FFFFFF"
  }
}));

const Sidebar = props => {
  const { variant, onClose, className, ...rest } = props;
  const [open, setOpen] = React.useState(true);
  const [show, setShow] = React.useState(false);
  const [open2, setOpen2] = React.useState(false);
  const [open3, setOpen3] = React.useState(false);
  const [open4, setOpen4] = React.useState(false);
  const [open5, setOpen5] = React.useState(false);


  const handleClick2 = () => {
    setOpen2((prev) => !prev);
  };

  const handleClickAway3 = () => {
    setOpen3(false);
  };
  const handleClick3 = () => {
    setOpen3((prev) => !prev);
  };

  const handleClickAway4 = () => {
    setOpen4(false);
  };
  const handleClick4 = () => {
    setOpen4((prev) => !prev);
  };

  const handleClickAway5 = () => {
    setOpen5(false);
  };
  const handleClick5 = () => {
    setOpen5((prev) => !prev);
  };

  const handleClickAway = () => {
    setOpen2(false);
  };


  const handleClick = () => {
    setOpen(prevOpen => !prevOpen);
  };
  const handleShow = () => {
    setShow(prevOpen => !prevOpen);
  };
  const classes = useStyles();

  const pages = [
    {
      title: 'Zone National',
      href: '/typography',
      open:{open},
       onClick:{handleClick},
      icon: <PublicIcon />
    },
    {
      title: 'Zones Internationales',
      href: '/dashboard',
      component:"li",
       in:{open},
        timeout:"auto",
       
      icon: <PublicIcon />
    },
    {
      title: 'Zones Risquées',
      href: '/zone',
      component:"li",
       in:{open},
        timeout:"auto",
       
      icon: <RoomIcon />
    },
  
  ];
  const pages2 = [
    {
      title: 'Articles Valides',
      href: '/icons',
      open:{open},
       onClick:{handleClick},
      icon: <DashboardIcon />
    },
    {
      title: 'Articles Non Valides',
      href: '/invalide',
      open:{open},
       onClick:{handleShow},
      icon: <DashboardIcon />
    },
    
    {
      title: 'Articles Non Lu',
      href: '/products',
      component:"li",
       in:{open},
        timeout:"auto",
        
       
      icon: <DashboardIcon />
    },
  
  ];
  const pages3 = [
   
    
    {
      title: 'Videos Non Lu',
      href: '/videoInt',
      component:"li",
       in:{open},
        timeout:"auto",
        
       
      icon: <VideocamIcon />
    },
    
  ];
  const pages4 = [
    {
      title: 'Posts Valides ',
      href: '/account',
      open:{open},
       onClick:{handleClick},
      icon: <LanguageIcon />
    },
    
      
    
    {
      title: 'Posts non Lu',
      href: '/settings',
      component:"li",
       in:{open},
        timeout:"auto",
        
       
      icon: <LanguageIcon />
    },
    {
      title: 'Parametres',
      href: '/users',
      component:"li",
       in:{open},
        timeout:"auto",
        
       
      icon: <SettingsIcon />
    },
    
  ];
  const pages5 = [
    {
      title: 'Cas Suspets Non Lu',
      href: '/casSuspect',
      open:{open},
       onClick:{handleClick},
      icon: <LocalHospitalIcon />
    },
   
    
  ];
 
  return (
    <Drawer
      anchor="left"
      classes={{ paper: classes.drawer }}
      onClose={onClose}
      open={open}
      variant={variant}
    >
      <div
        {...rest}
        className={clsx(classes.root, className)}
      >
     
        <Profile />
        <Divider className={classes.divider} />
        
        <SidebarNav
        className={classes.nav}
        pages={pages}
      />

        <ClickAwayListener onClickAway={handleClickAway}>
              <div className={classes.nav}>
              <div className={classes.nav} >
                <Button  className={classes.Button} type="button" onClick={handleClick2}>
                  Articles
                  {open2 ? (
                <ExpandLessIcon style={{ color: '#FFFFFF' }}></ExpandLessIcon>
                ):<ExpandMoreIcon style={{ color: '#FFFFFF' }}></ExpandMoreIcon>}
                </Button>
                
               </div>
                
                {open2 ? (
                  <div >
                  
                <SidebarNav
                className={classes.nav}
                pages={pages2}
              />
                  </div>
                ) : null}
              </div>
    </ClickAwayListener>


    <ClickAwayListener onClickAway={handleClickAway3}>
              <div>
              <div className={classes.nav} >
                <Button  className={classes.Button} type="button" onClick={handleClick3}>
                  Internautes
                  {open3 ? (
                <ExpandLessIcon style={{ color: '#FFFFFF' }}></ExpandLessIcon>
                ):<ExpandMoreIcon style={{ color: '#FFFFFF' }}></ExpandMoreIcon>}
                </Button>
               
               </div>
                {open3 ? (
                  <div >
                  
                <SidebarNav
                className={classes.nav}
                pages={pages3}
              />
                  </div>
                ) : null}
              </div>
    </ClickAwayListener>



    <ClickAwayListener onClickAway={handleClickAway4}>
              <div>
              <div className={classes.nav} >
                <Button  className={classes.Button} type="button" onClick={handleClick4}>
                  Robot
                  {open4 ? (
                <ExpandLessIcon style={{ color: '#FFFFFF' }}></ExpandLessIcon>
                ):<ExpandMoreIcon style={{ color: '#FFFFFF' }}></ExpandMoreIcon>}
                </Button>
                
               </div>
                {open4 ? (
                  <div >
                  
                <SidebarNav
                className={classes.nav}
                pages={pages4}
              />
                  </div>
                ) : null}
              </div>
    </ClickAwayListener>


    <ClickAwayListener onClickAway={handleClickAway5}>
              <div>
                
              <div className={classes.nav} >
                <Button  className={classes.Button} type="button" onClick={handleClick5}>
                  Cas Suspects
                  {open5? (
                <ExpandLessIcon style={{ color: '#FFFFFF' }}></ExpandLessIcon>
                ):<ExpandMoreIcon style={{ color: '#FFFFFF' }}></ExpandMoreIcon>}
                </Button>
               
               </div>
                {open5 ? (
                  <div >
                  
                <SidebarNav
                className={classes.nav}
                pages={pages5}
              />
                  </div>
                ) : null}
              </div>
    </ClickAwayListener>
       
      
      </div>
    </Drawer>
  );
};

Sidebar.propTypes = {
  className: PropTypes.string,
  onClose: PropTypes.func,
  open: PropTypes.bool.isRequired,
  variant: PropTypes.string.isRequired
};

export default Sidebar;