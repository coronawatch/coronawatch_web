import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import { Typography, Link } from '@material-ui/core';
import MailOutlineRoundedIcon from '@material-ui/icons/MailOutlineRounded';
import FacebookIcon from '@material-ui/icons/Facebook';
import CallIcon from '@material-ui/icons/Call';
import Grid, { GridSpacing } from '@material-ui/core/Grid';
const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(4),
    backgroundColor:'#C4C4C4',
  },
  link:{
    paddingTop: theme.spacing(2),
    paddingLeft: theme.spacing(7),
    
  },
  links:{
    color:'#FFFFFF',
  },
  text:{
    width: '289px',
height: '46px',
left: '932px',
top: '1049px',
    fontFamily: 'Roboto',
fontStyle: 'normal',
fontWeight: '500',
fontSize: '24px',
lineHeight: '28px',
display: 'flex',
alignItems: 'center',
textAlign: 'center',
letterSpacing:' 0.05em',

color: '#FFFFFF',
  }
}));

const Footer = props => {
  const { className, ...rest } = props;

  const classes = useStyles();
  

  return (
    <div
      {...rest}
      className={clsx(classes.root, className)}
    >
    <Typography className={classes.text} >
    Coordonnées de la cellule CC
    </Typography>
    <Grid className={classes.link} >
<MailOutlineRoundedIcon color="primary"/>
<Link href="#" className={classes.links} >
   ga_fedala@esi.dz
  </Link>
  </Grid>
  <Grid className={classes.link} >
<FacebookIcon  color="primary"/>
<Link href="#" className={classes.links}>
   ga_fedala@esi.dz
  </Link>
  </Grid>
  <Grid className={classes.link} >
<CallIcon color="primary"/>
<Link href="#" className={classes.links}>
   ga_fedala@esi.dz
  </Link>
  </Grid>
    </div>
  );
};

Footer.propTypes = {
  className: PropTypes.string
};

export default Footer;
