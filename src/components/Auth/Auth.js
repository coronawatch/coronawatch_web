import React, { useEffect, useState } from "react";
import { firebase } from "../../views/Dashboard/components/Firebase";
import { Switch, Redirect } from 'react-router-dom';
export const AuthContext = React.createContext(null);

export const AuthProvider = props => {
  const [currentUser, setCurrentUser] = useState(null);
  const [pending, setPending] = useState(true);
  
  useEffect(() => {
    firebase.auth().onAuthStateChanged((user) => {
      
      setCurrentUser(user)
     /* user.getIdToken.then((idToken)=>{
        console.log('token',idToken)
        return idToken
      })*/
      //console.log("the token", user.getIdToken())
      setPending(false)
    });
  }, []);
 

  if(pending){
    return <>Loading...</>
  }
  

  return (
    <AuthContext.Provider
      value={{
        currentUser
      }}
    >
      {props.children}
    </AuthContext.Provider>
  );
};