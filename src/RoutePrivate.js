
import React, { useContext } from "react";
import { Switch, Redirect } from 'react-router-dom';

import { RouteWithLayout } from './components';
import { AuthContext , AuthProvider} from './components/Auth';
import Routes from './Routes';


import { 
  ModeratorMain as MainLayout,
   AgentMain as AgentMainLayout,
   RedactorMain as RedactorMainLayout,
    Minimal as MinimalLayout,

} from './layouts';


import {
  ProductList as ProductListView,
  Icons as IconsView,
  ModeratorDashboard as DashboardView,
  AgentDashboard as AgentDashboardView,
  AgentNational as AgentNationalView,
  RedactorDashboard as RedactorDashboardView,
  ProductList as ProductListView,
  UserList as UserListView,
  Typography as TypographyView,
  Icons as IconsView,
  Account as AccountView,
  Settings as SettingsView,
  SignUp as SignUpView,
  SignIn as SignInView,
  NotFound as NotFoundView
} from './views';

const RoutePrivate = ({ component: RouteComponent, ...rest }) => {
  const {currentUser} = useContext(AuthContext);
  return (
    <RouteWithLayout>
      <AuthContext.Consumer>
      {value=>{
          return(
            !!currentUser ? (
                 
    <Switch>
    <Redirect
      exact
      from="/"
      to="/dashboard"
    />
     <RouteWithLayout
        component={ProductListView}
        exact
        layout={MainLayout}
        path="/products"
      />
        <RouteWithLayout
        component={IconsView}
        exact
        layout={MainLayout}
        path="/icons"
      />
    <RouteWithLayout
      component={DashboardView}
      exact
      layout={MainLayout}
      path="/dashboard"
    />
    <RouteWithLayout
      component={AgentNationalView}
      exact
      layout={AgentMainLayout}
      path="/agentNational"
    />
    <RouteWithLayout
      component={AgentDashboardView}
      exact
      layout={AgentMainLayout}
      path="/agent"
    />
    <RouteWithLayout
      component={RedactorDashboardView}
      exact
      layout={RedactorMainLayout}
      path="/redactor"
    />
    <RouteWithLayout
      component={UserListView}
      exact
      layout={MainLayout}
      path="/users"
    />
    <RouteWithLayout
      component={ProductListView}
      exact
      layout={MainLayout}
      path="/products"
    />
    <RouteWithLayout
      component={TypographyView}
      exact
      layout={MainLayout}
      path="/typography"
    />
    <RouteWithLayout
      component={IconsView}
      exact
      layout={MainLayout}
      path="/icons"
    />
    <RouteWithLayout
      component={AccountView}
      exact
      layout={MainLayout}
      path="/account"
    />
    <RouteWithLayout
      component={SettingsView}
      exact
      layout={MainLayout}
      path="/settings"
    />
    <RouteWithLayout
      component={SignUpView}
      exact
      layout={MinimalLayout}
      path="/sign-up"
    />
    <RouteWithLayout
      component={SignInView}
      exact
      layout={MinimalLayout}
      path="/sign-in"
    />
    <RouteWithLayout
      component={NotFoundView}
      exact
      layout={MinimalLayout}
      path="/not-found"
    />
    <Redirect to="/not-found" />
  </Switch>
              ) : (
                 
    <Switch>
   
  
    <RouteWithLayout
      component={SignInView}
      exact
      layout={MinimalLayout}
      path="/sign-in"
    
    />
    <Redirect to="/sign-in" />
  </Switch>
              )
          )
      }}
      </AuthContext.Consumer>
    </RouteWithLayout>
     
    
  );
};

export default RoutePrivate;
