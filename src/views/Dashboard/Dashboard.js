import React, { useState, useEffect,useContext}from "react";
import { makeStyles } from '@material-ui/styles';
import { Grid } from '@material-ui/core';
import { SearchInput } from 'components';
import axios from 'axios'
import {firebase} from "../Dashboard/components/Firebase/index";
import { AuthContext , AuthProvider} from '../../components/Auth';
import ReactDOM from "react-dom";
import ReactTooltip from "react-tooltip";
import $ from 'jquery';
import {
Table,
  Budget,
   Validate,
  TotalUsers,
  TasksProgress,
  TotalProfit,
} from './components';
import MapChart from "./components/MapChart";


const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(1),
  },
  row: {
    width:'80%',
    display: 'flex',
    alignItems: 'center',
  
    margin: theme.spacing(2)
  },
 title:{
  fontFamily: 'Roboto',
  fontStyle: 'normal',
fontWeight: 'bold',
   fontSize:'24px',
   display: 'flex',
   alignItems: 'center',
 marginTop:'2%',
   marginLeft: '5%',

 }
}));

const Dashboard = () => {
  const { currentUser } = useContext(AuthContext);
  async function token() {
     return await firebase.auth().currentUser.getIdToken()
    
  } 
  
const classes = useStyles();
const [content, setContent] = useState("");
const [count, setCount] = useState([]);

async function data (){   
  const auth=  await token() 
  const res = await axios.get(`/api/carte/international?pays=${content}&extra`,{  headers: { Authorization: `Bearer ${auth}` }})
  
      setCount(res.data);
      
      console.log(res);
   
}
useEffect(()=>{
data()
},[content]
)

  return (
 
    <div>
      <p className={classes.title}>
          {content}
      </p>
   
      <Grid container>
      <Grid item
          lg={9}
          sm={6}>
         
          <MapChart setTooltipContent={setContent} />
    
      </Grid>
      <Grid item  lg={3} sm={6}>
      <Grid 
        className={classes.root}
          item
          lg={12}
          sm={12}
          xl={3}
          xs={12}
        >
          <TotalProfit  Pays={content} count={count}  />
 
          </Grid>
   
      <Grid  className={classes.root}
      
          item
          lg={12}
          sm={12}
          xl={3}
          xs={12}
        >
          <TotalUsers   Pays={content} count={count} />
          </Grid>
          <Grid className={classes.root}
   
          item
          lg={12}
          sm={12}
          xl={3}
          xs={12}
        >
          <TasksProgress  Pays={content} count={count} />
          </Grid>
          
          <Grid className={classes.root}
      
          item
          lg={12}
          sm={12}
          xl={3}
          xs={12}
         
        >
          <Budget Pays={content} count={count} />
      </Grid>
      </Grid>
      </Grid>
     <Validate  Pays={content} count={count} nat={"True"} />
<Table/>
          </div>

  );
  
};

export default Dashboard;
