import React , { useState, useEffect }  from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { Link as RouterLink } from 'react-router-dom';
import { makeStyles } from '@material-ui/styles';
import { Card, CardContent, Grid, Typography } from '@material-ui/core';
import axios from "axios";

const useStyles = makeStyles(theme => ({
  root: {
    height: '100%',
    width:'90%'
  },
  valide: {
    position: 'absolute',
    left: '93%',
    right: '2.5%',
    top: '27%',
    bottom: '63%',
    width:'30px',
    height:'30px',
    },
h4:{
  fontFamily: 'Roboto',
fontStyle: 'normal',
fontWeight: '0',
fontSize: '20px',
lineHeight: '23px',
textAlign: 'center'

}


}));

const Budget = props => {
  const { className, ...rest } = props;

  const classes = useStyles();
  
  if ( props.count.status=="NONLU")
  {
    return (
      <Card
        {...rest}
        className={clsx(classes.root, className,)}
      >
        <CardContent>
          <Grid
            container
            justify="space-between"
          >
            <Grid item>
            <Typography variant="h1" align="center" > 
            {
                 
                 props.count.nbConfirme
              }
            
            </Typography>
              <Typography 
                variant="h4"
                align="left"   
              >
                 
            <img 
              alt="Logo"
              src="/images/icons/secured1.png"
              className={classes.image}
            />
         
         cas confirmés
              
        </Typography>
            </Grid>
           
          </Grid>
      
        </CardContent>
      </Card>
    );

  }else if( props.count.status=="VALIDE"){
    return (
      <Card
        {...rest}
        className={clsx(classes.root, className,)}
      >
        <CardContent>
          <Grid
            container
            justify="space-between"
          >
            <Grid item>
            <Typography variant="h1" align="center" > 
            {
                 
                 props.count.nbConfirme
              }
            
            </Typography>
            <img 
              alt="icon"
              src="/images/icons/valide.png"
              className={classes.valide}
            />
              <Typography 
                variant="h4"
                align="left" 
           
                
              >
                 
            <img s
              alt="Logo"
              src="/images/icons/secured1.png"
              className={classes.image}
            />
         
         cas confirmés
              
        </Typography>
            </Grid>
           
          </Grid>
      
        </CardContent>
      </Card>
    );

  }else if ( props.count.status=="INVALIDE")
  {
    return (
      <Card
        {...rest}
        className={clsx(classes.root, className,)}
      >
        <CardContent>
          <Grid
            container
            justify="space-between"
          >
            <Grid item>
            <Typography variant="h1" align="center" > 
            {
                 
                 props.count.nbConfirme
              }
            
            </Typography>
            <img 
              alt="icon"
              src="/images/icons/delete.png"
              className={classes.valide}
            />
              <Typography 
                variant="h4"
                align="left" 
           
                
              >
                 
            <img s
              alt="Logo"
              src="/images/icons/secured1.png"
              className={classes.image}
            />
         
         cas confirmés
              
        </Typography>
            </Grid>
           
          </Grid>
      
        </CardContent>
      </Card>
    );

  }else 
  { return (
   " "
);
  }
 
};

Budget.propTypes = {
  className: PropTypes.string
};

export default Budget;
