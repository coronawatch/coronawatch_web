import firebase from "firebase";
import "firebase/storage";
import 'firebase/firestore';

import 'firebase/auth';
require('firebase/auth')

// Your web app's Firebase configuration
var firebaseConfig = {
    apiKey: "AIzaSyAeF4WOGZQCY1KmS4VxHtBQxp7AiS4ltbY",
    authDomain: "coronawatch-62206.firebaseapp.com",
    databaseURL: "https://coronawatch-62206.firebaseio.com",
    projectId: "coronawatch-62206",
    storageBucket: "coronawatch-62206.appspot.com",
    messagingSenderId: "776747483591",
    appId: "1:776747483591:web:78ec31a8eb4242de4659e2",
    measurementId: "G-Z2QGLVM44N"

};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

const storage = firebase.storage();
const auth = firebase.auth();

export { storage, firebase,auth as default };

   