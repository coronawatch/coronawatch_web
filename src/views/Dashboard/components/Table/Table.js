import React,{useEffect,useState,useContext} from 'react';
import MaterialTable from 'material-table';
import { forwardRef } from 'react';
import axios from "axios";
import { Grid } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import Button from '@material-ui/core/Button';

import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import { SimpleInput } from 'components';
import Modal from '@material-ui/core/Modal';

import { AuthContext , AuthProvider} from '../../../../components/Auth';
import {firebase} from "../Firebase/index";

import AddBox from '@material-ui/icons/AddBox';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';

const useStyles = makeStyles(theme => ({
  root: {
    height: '100%',
    width:'100%',
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: '13px',
    lineHeight: '15px',
    alignItems: 'center',
  },
 
  title: {
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize:'24px',
    display: 'flex',
    alignItems: 'center',
    marginTop:'5%',
    marginLeft: '5%',
    marginBottom:'5%' 
  },
  labels: {
   color: '#A3A3A3'
  },
  paper: {
    position: 'absolute',
    backgroundColor: theme.palette.background.paper,
    border: '0 px',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
  row: {
  marginBottom: '20px'  
  }



}));
const tableIcons = {
    Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
    Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
    Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
    DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
    Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
    LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
    NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
    ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
    SortArrow: forwardRef((props, ref) => <ArrowDownward {...props} ref={ref} />),
    ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />)
  };
 

export default function Table() {
  
    const [users,setUsers]=useState([])
    const [id,setId]=useState('')
    const [etat,setEtat]=useState('')
    const [detail,setDetails]=useState({
      roles:{
        ADMIN:'false',
        MODERATEUR:'false',
        AGENT_DE_SANTE:'false',
        MEMBRE_DE_CCC:'false',

        REDACTEUR:'false',
        
            },
    })
    const classes = useStyles()
  
  
  
    
    const data =()=>{
      const j = token().then((value)=>{
        console.log('calue',value)
         axios.get('/api/carte/international',{ headers: { Authorization: `Bearer ${value}`} })
         .then(
           res=>{
             
             setUsers(res.data)
             console.log(res)
             console.log(res.data)
            // console.log(users)
             setState((prevState) => {
                const data= res.data;
                return { ...prevState, data };
              });
           }
             
         )
          })
       }
       useEffect(()=>{
       data()
      
     },[])
  const [state, setState] = React.useState({
    columns: [
      { title: 'Pays', field: 'country' },
      { title: 'Porteurs', field: 'data.nbPorteurs', type: 'email' },
      { title: ' Cas Confirmés', field: 'data.nbConfirme'},
      { title: ' Décés', field: 'data.nbDeces'},
      { title: ' Cas Suspect', field: 'data.nbSuspect'},
      { title: ' Guérisons', field: 'data.nbRetablie'},

    ],
 // field selon l'objet   
    data: users
  });


  const { currentUser } = useContext(AuthContext);
  async function token() {
    return await firebase.auth().currentUser.getIdToken()//.then((idToken)=>{
     //console.log('token',idToken)
     //return idToken
   //})
   
 } 
  return (
    <div>
    <MaterialTable
      title="Editable Example"
      columns={state.columns}
      data={state.data}
      icons={tableIcons}
     
      editable={{
        onRowAdd: (newData) =>
        
          new Promise((resolve) => {
            console.log('newdata',newData)
            axios.post(`http://7abdf0a7.ngrok.io/api/users`, newData, { headers:  token() })
            .then(res => {
              console.log(res);
              console.log(res.data);
            }).catch((error) => console.log(error));
            setTimeout(() => {
              resolve();
              setState((prevState) => {
                const data = [...prevState.data];
                data.push(newData);
                return { ...prevState, data };
              });
             
             
              
            }, 600);
          }),
        onRowUpdate: (newData, oldData) =>
          new Promise((resolve) => {
            setTimeout(() => {
              resolve();
              if (oldData) {
                setState((prevState) => {
                  const data = [...prevState.data];
                  data[data.indexOf(oldData)] = newData;
                  return { ...prevState, data };
                });
              }
            }, 600);
          }),
        onRowDelete: (oldData) =>
        
          new Promise((resolve) => {
            console.log('newdata',oldData)
          

            const j = token().then((value)=>{
              console.log('calue',value)
               axios.delete(`http://14576360e72114576360e721.ngrok.io/api/users/${oldData.uid}`,{ headers: { Authorization: `Bearer ${value}`} })
               .then(
                 res=>{ console.log(res)})
                })

            setTimeout(() => {
              resolve();
              setState((prevState) => {
                const data = [...prevState.data];
                data.splice(data.indexOf(oldData), 1);
                return { ...prevState, data };
              });
            }, 600);
          }),


          
      }}
      
    />
   
    
    </div>
  );
}