import React , { useState, useEffect }from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { Link as RouterLink } from 'react-router-dom';
import { makeStyles } from '@material-ui/styles';
import { Card, CardContent, Grid, Typography, Avatar } from '@material-ui/core';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import MoneyIcon from '@material-ui/icons/Money';
import axios from "axios";
const useStyles = makeStyles(theme => ({
  root: {
    height: '100%',
    width:'90%',
    paddingLeft:"3%",
  },
 

  delete:{
    position: 'absolute',
    left: '93%',
    right: '2.5%',
    top: '73%',
    
    width:'30px',
    height:'30px',
  },
  valide: {
    position: 'absolute',
    left: '93%',
    right: '2.5%',
    top: '65%',
    
    width:'30px',
    height:'30px',
    },
  image:{
    height:"40%",
    width:"40%"
  },
  nbr:{
    paddingLeft:"40%"
    
        },



}));

const Budget = props => {
  const { className, ...rest } = props;

  const classes = useStyles();

  if ( props.count.status=="NONLU"){
    return (
      <Card
        {...rest}
        className={clsx(classes.root, className)}
      >
        <CardContent>
          <Grid
            container
            justify="space-between"
          >
            <Grid item>
            <Typography variant="h1" 
            className={classes.nbr}
            > 
            {
                 
                 props.count.nbDeces
              }
            
            </Typography>
              <Typography
                variant="h3"
                align="left" 
                className={classes.title}
                
              >
                 
            <img 
              alt="icon"
              src="/images/icons/patient1.png"
              className={classes.image}
            />
         
         Décés
              
        </Typography>
            </Grid>
           
          </Grid>
      
        </CardContent>
      </Card>
    );

  }else if ( props.count.status=="VALIDE"){
    return (
      <Card
        {...rest}
        className={clsx(classes.root, className)}
      >
        <CardContent>
          <Grid
            container
            justify="space-between"
          >
            <Grid item>
            <Typography variant="h1" align="center" className={classes.nbr}> 
            {
                 
                 props.count.nbDeces
              }
            
            </Typography>
            <img 
              alt="icon"
              src="/images/icons/valide.png"
              className={classes.delete}
            />
              <Typography
                variant="h3"
                align="left" 
                className={classes.title}
                
              >
                 
            <img 
              alt="icon"
              src="/images/icons/patient1.png"
              className={classes.image}
            />
         
                    Décés
              
        </Typography>
            </Grid>
           
          </Grid>
      
        </CardContent>
      </Card>
    );

  }else if( props.count.status=="INVALIDE"){
    return (
      <Card
        {...rest}
        className={clsx(classes.root, className)}
      >
        <CardContent>
          <Grid
            container
            justify="space-between"
          >
            <Grid item>
            <Typography variant="h1" align="center" className={classes.nbr}> 
            {
                 
                 props.count.nbDeces
              }
            
            </Typography>
            <img 
              alt="icon"
              src="/images/icons/delete.png"
              className={classes.delete}
            />
              <Typography
                variant="h3"
                align="left" 
                className={classes.title}
              >
                 
            <img 
              alt="icon"
              src="/images/icons/patient1.png"
              className={classes.image}
            />
         
             Décés
              
        </Typography>
            </Grid>
           
          </Grid>
      
        </CardContent>
      </Card>
    );

  }else 
  { return (
   " "
);
  }
 
};

Budget.propTypes = {
  className: PropTypes.string
};

export default Budget;
