import React,{ useState, useEffect }  from 'react';
import clsx from 'clsx';
import Button from '@material-ui/core/Button';
import PropTypes from 'prop-types';
import { Link as RouterLink } from 'react-router-dom';
import { makeStyles } from '@material-ui/styles';
import { Card, CardContent, Grid, Typography, Avatar,Link } from '@material-ui/core';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import MoneyIcon from '@material-ui/icons/Money';
import { width } from '@material-ui/system';
import axios from 'axios'
const useStyles = makeStyles(theme => ({
  root: {
width: '747px',
height: '60px',
marginLeft:'5%',
marginBottom:'5%',
background: '#FFFFFF',
boxShadow:' 0px 5.5px 5px rgba(0, 0, 0, 0.24), 0px 9px 18px rgba(0, 0, 0, 0.18)',
borderRadius: '6px',
  },
 delete:{
  marginLeft:"2%",
  color: '#C12020',
 },
 valide:{
marginLeft:"5%",
  color: '#4EACAC',
  
 },
 image:{
  position:' absolute',
  width: '25px',
  height: '25px',
  left: '617px',
},
  image2:{
position: 'absolute',
width: '25px',
height: '25px',
left: '994px', 
 },
 img:{
  
  marginLeft:"45%"
}



}));



const Validate = props => {
  const [showText, setShowText] = React.useState(false)
  const [showImg, setShowImg] = React.useState()
  useEffect(()=>{
    setShowText(false);
    },[props.Pays])
  const valide = () =>{

    const True="true";

    if (props.nat=="True"){
    axios.put(`http://7abdf0a7.ngrok.io/api/carte/international?country=${props.Pays}&validate=${True}`)
    .then(()=>{ 
      setShowText(true);
      setShowImg(true)
    });
    }else{
      axios.put(`http://7abdf0a7.ngrok.io/api/carte/national?wilaya=${props.Pays}&validate=${True}`)
      .then(()=>{ 
       
        setShowText(true);
      setShowImg(true)
        });
    }
    axios.get(`https://us-central1-coronawatch-62206.cloudfunctions.net/notificationCasSuspect?type=map&token=cGcA6eNFTTC-l3jdV-vFHM:APA91bEq1-7dwZeEqK0Y-K8G9Wdvzsgu6mp1oR4_fMuFqgooIZekyFfAQLCSU9WVD_tc-rKTtUHNJwsRM629dZqs_oOZLFUoQlY-Ez0aKjzbiml0F6GyW2e1Tcv0--5OOT_zjnCtYAbN&title=sdfsdfsdfsd&body=sdfsfsdfs`);

 
    
 };
 const deletes = () => {
  console.log(props.Pays)
  const False="false";
  if (props.nat=="True"){
    axios.put(`http://7abdf0a7.ngrok.io/api/carte/international?country=${props.Pays}&validate=${False}`)
    .then(()=>{ 
      setShowText(true);
      setShowImg(false)
    });
    }else{
      axios.put(`http://7abdf0a7.ngrok.io/api/carte/national?wilaya=${props.Pays}&validate=${False}`)
      .then(()=>{ 
        setShowText(true);
        setShowImg(false)
        });
    }
 }
 
  const { className, ...rest } = props;

   const classes = useStyles();
   const a=2;
  if ( props.count.status=="NONLU"){
  return (<div>
   {!showText &&  <Card
      {...rest}
      className={clsx(classes.root, className)}
    >
      <CardContent>
  
           <Button
           
           onClick={deletes}
        variant="contained"
        color="#000000"
        className={classes.button}
        className={classes.delete}
        startIcon={ <img 
           
            src="/images/icons/delete.png"
          
          />}
      >

               Supprimer la nouvelle information
         </Button>
 
    <Button
           onClick={valide}
        variant="contained"
        color="#000000"
        onClick={valide}
        className={classes.button}
        className={classes.valide}
        startIcon={ <img 
         
          src="/images/icons/valide.png"
          
        />}
      >
        Valider la nouvelle information
      </Button> 
      </CardContent>
    </Card>}
    {showText &&  <Card
      {...rest}
      className={clsx(classes.root, className)}
    >
     <CardContent>
    
        {showImg && <img  className={classes.img}  src="/images/icons/valide.png"></img>}
        {!showImg && <img  className={classes.img}  src="/images/icons/delete.png"></img>}
    </CardContent>
    </Card>}
    </div>
  );}
  else {
    return (
      ""
    )
  }
};

Validate.propTypes = {
  className: PropTypes.string
};

export default Validate;
