import React, { useState, useEffect } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { Link as RouterLink } from 'react-router-dom';
import { makeStyles } from '@material-ui/styles';
import { Card, CardContent, Grid, Typography, Avatar } from '@material-ui/core';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import MoneyIcon from '@material-ui/icons/Money';
import { width } from '@material-ui/system';
import axios from "axios";
import { exportDefaultSpecifier } from '@babel/types';


const useStyles = makeStyles(theme => ({
  root: {
    height: '100%',
    width:'90%'
  },
  title: {
  fontSize:'900'  
  },
  valide: {
    position: 'absolute',
    left: '93%',
    right: '2.5%',
    top: '95%',
 
    width:'30px',
    height:'30px',
    },
 
  


}));

const Budget = props => {
  const { className, ...rest } = props;

  const classes = useStyles();
 
 
  if (props.count.status=="NONLU"){
    return (

      <Card
        {...rest}
        className={clsx(classes.root, className)}
      >
        <CardContent>
          <Grid
            container
            justify="space-between"
          >
            <Grid item>
            <Typography variant="h1" align="center" > 
               {
                 props.count.nbSuspect
               }
          
            
            </Typography>
              <Typography
                variant="h3"
                align="left" 
                className={classes.title}
                
              >
                 
            <img 
              alt="icon"
              src="/images/icons/hospital1.png"
              className={classes.image}
            />
         
                Cas Suspect
              
        </Typography>
            </Grid>
           
          </Grid>
      
        </CardContent>
      </Card>
    );

  } else if(props.count.status=="VALIDE")
  {
    return (
      <Card
        {...rest}
        className={clsx(classes.root, className)}
      >
        <CardContent>
          <Grid
            container
            justify="space-between"
          >
            <Grid item>
            <Typography variant="h1" align="center" >
            { props.count.nbSuspect}
            
            </Typography>
            <img 
              alt="icon"
              src="/images/icons/valide.png"
              className={classes.valide}
            />
              <Typography
                variant="h3"
                align="left" 
                className={classes.title} 
              >   
            <img 
              alt="icon"
              src="/images/icons/hospital1.png"
              className={classes.image}
            />
         
                Cas Suspect
              
        </Typography>
            </Grid>
           
          </Grid>
      
        </CardContent>
      </Card>
    );

  } else if(props.count.status=="INVALIDE")
  {
    return (
      <Card
        {...rest}
        className={clsx(classes.root, className)}
      >
        <CardContent>
          <Grid
            container
            justify="space-between"
          >
            <Grid item>
            <Typography variant="h1" align="center" >
            { props.count.nbSuspect}
            </Typography>
            <img 
              alt="icon"
              src="/images/icons/delete.png"
              className={classes.valide}
            />
              <Typography
                variant="h3"
                align="left" 
                className={classes.title} 
              >   
            <img 
              alt="icon"
              src="/images/icons/hospital1.png"
              className={classes.image}
            />
         
                Cas Suspect
              
        </Typography>
            </Grid>
           
          </Grid>
      
        </CardContent>
      </Card>
    );
  

  }else 
  { return (
  " "
);
  }
 
};

Budget.propTypes = {
  className: PropTypes.string
};

export default Budget;
