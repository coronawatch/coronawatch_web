import React, { useState, useEffect } from 'react';
import { Link as RouterLink, withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import validate from 'validate.js';
import { makeStyles } from '@material-ui/styles';
import {
  Grid,
  Button,
  IconButton,
  TextField,
  Link,
  Typography
} from '@material-ui/core';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';

import { Facebook as FacebookIcon, Google as GoogleIcon } from 'icons';

const schema = {
  email: {
    presence: { allowEmpty: false, message: 'is required' },
    email: true,
    length: {
      maximum: 64
    }
  },
  password: {
    presence: { allowEmpty: false, message: 'is required' },
    length: {
      maximum: 128
    }
  }
};

const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: 'white',
    height: '100%'
  },
  grid: {
    height: '100%'
  },
  quoteContainer: {
    [theme.breakpoints.down('md')]: {
      display: 'none'
    }
  },
  quote: {
  
    height: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundImage: 'url(/images/login1.png)',
    
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center'
  },

 
  name: {
    marginTop: theme.spacing(3),
    color: theme.palette.white
  },
  bio: {
    color: theme.palette.white
  },
  contentContainer: {},
  content: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column'
  },
  contentHeader: {
    display: 'flex',
    alignItems: 'center',
    paddingTop: theme.spacing(5),
    paddingBototm: theme.spacing(2),
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2)
  },

  contentBody: {
    flexGrow: 1,
    display: 'flex',
    alignItems: 'center',
    [theme.breakpoints.down('md')]: {
      justifyContent: 'center'
    }
  },
  form: {
    paddingLeft: 100,
    paddingRight: 100,
    paddingBottom: 125,
    flexBasis: 700,
    [theme.breakpoints.down('sm')]: {
      paddingLeft: theme.spacing(2),
      paddingRight: theme.spacing(2)
    }
  },
  title: {
    marginTop: theme.spacing(3),
  marginLeft: theme.spacing(13),
  
    width: '247px',
    height: '43px',
    left:' 812px',
 
    
    fontFamily: 'Roboto',
    fontStyle:' normal',
    fontWeight:' bold',
    fontSize: '40px',
    lineHeight:' 47px',
    display: 'flex',
    alignItems:' center',
    
    color: '#2A204E',
    
  
  },
  mdps:{
   
    width: '350px',
height: '58px',
left: '760px',
top: '352px',

fontFamily: 'Roboto',
fontStyle: 'normal',
fontWeight: 'normal',
fontSize: '13px',
lineHeight: '15px',
display:' flex',
alignItems: 'center',
letterSpacing: '0.05em',

color: '#000000',

  },
 stitle:{
  marginTop: theme.spacing(3),
  marginLeft: theme.spacing(12), 
  width: '270px',
  height: '58px',
  left: '800px',
  top: '252px',
  
  fontFamily: 'Roboto',
  fontStyle: 'normal',
  fontWeight: 'normal',
  fontSize: '24px',
  lineHeight: '28px',
  display: 'flex',
  alignItems: 'center',
  textAlign: 'center',
  letterSpacing: '0.05em',
  
  color: '#000000',
},
  socialIcon: {
    marginRight: theme.spacing(1)
  },
  sugestion: {
    marginTop: theme.spacing(2)
  },
  textField: {
    marginTop: theme.spacing(),
   
  
   
   
  },
  signInButton: {
    margin: theme.spacing(2, 0),
    background: 'linear-gradient(275.22deg, #27F0F0 0%, rgba(255, 255, 255, 0) 50%), #303395',
opacity: '0.7',
border: '1px solid #DFE2E6',
boxSizing: 'border-box',
boxShadow:' 0px 0px 3px rgba(0, 0, 0, 0.05)',
borderRadius: '25px',
fontFamily: 'Roboto',
fontStyle: 'normal',
fontWeight: 'normal',
marginTop: theme.spacing(5),
color:"#FFFFFF",
"&:disabled": { marginTop: theme.spacing(5),
  background: 'linear-gradient(275.22deg, #27F0F0 0%, rgba(255, 255, 255, 0) 50%), #303395',
  opacity: '0.7',
  border: '1px solid #DFE2E6',
  boxSizing: 'border-box',
  boxShadow:' 0px 0px 3px rgba(0, 0, 0, 0.05)',
  borderRadius: '25px',
  fontFamily: 'Roboto',
  fontStyle: 'normal',
  fontWeight: 'normal',
 
  color:"#FFFFFF",
}
  },

}));

const SignIn = props => {
  const { history } = props;

  const classes = useStyles();

  const [formState, setFormState] = useState({
    isValid: false,
    values: {},
    touched: {},
    errors: {}
  });

  useEffect(() => {
    const errors = validate(formState.values, schema);

    setFormState(formState => ({
      ...formState,
      isValid: errors ? false : true,
      errors: errors || {}
    }));
  }, [formState.values]);

  const handleBack = () => {
    history.goBack();
  };

  const handleChange = event => {
    event.persist();

    setFormState(formState => ({
      ...formState,
      values: {
        ...formState.values,
        [event.target.name]:
          event.target.type === 'checkbox'
            ? event.target.checked
            : event.target.value
      },
      touched: {
        ...formState.touched,
        [event.target.name]: true
      }
    }));
  };

  const handleSignIn = event => {
    event.preventDefault();
    history.push('/');
  };

  const hasError = field =>
    formState.touched[field] && formState.errors[field] ? true : false;

  return (
    <div className={classes.root}>
      <Grid
        className={classes.grid}
        container
      >
        <Grid
          className={classes.quoteContainer}
          item
          lg={5}
        >
          <div className={classes.quote}>
         
          </div>
        </Grid>
        <Grid
          className={classes.content}
          item
          lg={7}
          xs={12}
        >
          <div className={classes.content}>
         
            <div className={classes.contentBody}>
              <form
                className={classes.form}
                onSubmit={handleSignIn}
              >
                <Typography
                  className={classes.title}
                  variant="h2"
                >
                  CoronaWatch
                </Typography>
                <Typography
                  color="textSecondary"
                  gutterBottom
                  className={classes.stitle}
                >
              
              Récupérer votre compte
                </Typography>
          
                <Typography
                  color="#000000"
                  variant="body1"
                  className={classes.mdps}
                >
                 
    
                 Merci de vérifier que vous avez reçu un courriel comportant votre code. Celui-ci est composé de 6 chiffres.
                
                </Typography>
                <TextField
                  className={classes.textField}
                  error={hasError('email')}
                  fullWidth
                  helperText={
                    hasError('email') ? formState.errors.email[0] : null
                  }
                  label="Email address"
                  name="email"
                  onChange={handleChange}
                  type="text"
                  value={formState.values.email || ''}
                  variant="outlined"
                />
             
                <Button 
                  className={classes.signInButton}
                  
                  disabled={!formState.isValid}
                  fullWidth
                  size="large"
                  type="submit"
                  variant="contained"
                 
                >
                 Continuer
                </Button>
                <Typography
                  color="#303395"
                  variant="body1"
                  className={classes.mdps2}
                >
                 
                  <Link
                    component={RouterLink}
                    to="/sign-up"
                    variant="h6"
                  >
                    code non reçu ?
                  </Link>
                </Typography>
              </form>
            </div>
          </div>
        </Grid>
      </Grid>
    </div>
  );
};

SignIn.propTypes = {
  history: PropTypes.object
};

export default withRouter(SignIn);
