import React, { useState, useEffect,useContext }  from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import {firebase} from "../../Dashboard/components/Firebase/index";
import { AuthContext , AuthProvider} from '../../../components/Auth';
import Button from '@material-ui/core/Button';
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';
import ArrowBackRoundedIcon from '@material-ui/icons/ArrowBackRounded';
import axios from 'axios';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import { makeStyles } from '@material-ui/styles';
import styled from 'styled-components'
import {
  Card,
  CardContent,
  CardActions,
  Typography,
  Grid,
  Divider
} from '@material-ui/core';

import Modal from '@material-ui/core/Modal';
const useStyles = makeStyles(theme => ({
  card:{
      marginTop:"2%"
  },
  title: {
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize:'24px',
    display: 'flex',
    alignItems: 'center',
    marginTop:'5%',
    marginLeft: '5%',
    marginBottom:'5%' 
  },
   name:{
    
    height:" 20px",
    fontFamily: "Roboto",
    fontStyle: "normal",
    fontWeight: "500",
    fontSize: "19px",
    color: "#4EACAC",
   },
   contenu:{
     marginTop:"1%",
    fontFamily: "Roboto",
    fontStyle: "normal",
    fontWeight: "normal",
    fontSize: "12px",
    lineHeight: "14px",
    textAlign: "justify",
    letterSpacing: "0.05em",
    color: "#000000",

   },
   paper: {
    position: 'absolute',
    backgroundColor: theme.palette.background.paper,
    border: '0 px',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
  row: {
    marginBottom: '20px'  
    }
  
  
  }));
  function getModalStyle() {
    const top = 50;
    const left = 50 ;  
    return {
      top: '50%',
      left: '50%',
      transform: 'translate(-50%, -50%)',
      width: '52%',
      padding: '0%',
      borderRadius: '6px',
    };
  }
const PageValide= props=> {

    const classes = useStyles();
    const { className, ...rest } = props;
    const [comment, setComment] = useState([]);
    const [open, setOpen] = React.useState(false);
    const [modalStyle] = React.useState(getModalStyle);
    const [showText, setShowText] = React.useState(false)
    const { currentUser } = useContext(AuthContext);
    async function token() {
       return await firebase.auth().currentUser.getIdToken()
      
    } 
    useEffect(() => {
       console.log(props.id)
       async function fetchPosts(){
 
        const auth=  await token() 
 
        const res= await axios.get(`http://157bb3e084f8.ngrok.io/api/articles/${props.id}/comments`,{ headers: { Authorization: `Bearer ${auth}` }});
    
       setComment(res.data)
  
      
      }
      fetchPosts();
      
     },[]);
     async function valide (id){
      const auth=  await token() 
       setShowText(true)
      console.log(id)
      const res= await axios.delete(`http://157bb3e084f8.ngrok.io/api/articles/${props.id}/comments/${id}`,{ headers: { Authorization: `Bearer ${auth}` }})
  
     // await axios.get(`https://jsonplaceholder.typicode.com/posts/1`)
     
  
   };
     const Delete = () =>{
      setOpen(true);
   };
  
  

  const handleClose = () => {
    setOpen(false);
  }; 
   const falses =()=>{
    props.show(false);
  }
  return (<div>
    <ArrowBackRoundedIcon
    color="#4EE2EC"
    onClick={falses}
    />
<Card
      {...rest}
      className={clsx(classes.root, className)}
     
       
      >
    <CardContent
       
    >
<div dangerouslySetInnerHTML={{__html: props.postId.contenu}}></div>
        <hr align="center" width="100%" color="#EEEEEE" size="5" />
        <h6>Commentaires</h6>
        <p >{ comment.length} commentaires</p>
        {
  
        comment.map(
               
          cmnt => ( 
             <Card  className={classes.card}>
             <CardContent
             
             >
                 <Modal open={open} onClose={handleClose} aria-labelledby="simple-modal-title" >
          <Grid container direction="row" justify="center" alignItems="center">
            <div style={modalStyle} className={classes.paper}>
              <Grid container direction="row" justify="center" alignItems="center">
                <h2 className={classes.title} id="simple-modal-title">
                    vous voulez vraiment supprimer le commentaire
                </h2>

              </Grid>                
              <div className={classes.row}>
                <Grid  container direction="row"  justify="center" alignItems="center" >
                 <Button
                 onClick={()=>valide(cmnt.id)}
                 >
                  Oui
                 </Button>
                 
                 <Button>
                
                 Non
                 </Button>
                </Grid>
              
              </div>
            </div>
          </Grid> 
          </Modal>        
               <Grid
               container
               direction="row"
               >
               <Grid
                 lg={11}
                 md={11}
                 xs={8}>
             <Typography
              className={classes.name}
             >
               {cmnt.detail.user.name}
             </Typography>
             </Grid>
             <Grid
               lg={1}
               md={1}
               xs={4}>
            { !showText && <DeleteOutlineIcon
             onClick={Delete}
             align="right"
             />}
           
             {
               showText && <HighlightOffIcon
               align="right"
               />
             }
             </Grid>
             </Grid>
             <Typography
              className={classes.contenu}
             >
               {cmnt.detail.contenu}
             </Typography>
             
             </CardContent>
             </Card>
         
            ))
          
            }
      

    </CardContent>
     
</Card>
</div>
 )

  }
export default PageValide;
