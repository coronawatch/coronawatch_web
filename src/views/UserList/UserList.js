import React, { useState, useEffect }  from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import { Paper, Input } from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import axios from 'axios'
import {
  Card,
  CardContent,
  CardActions,
  Typography,
  Grid,
  Divider
} from '@material-ui/core';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import { UsersToolbar, UsersTable } from './components';
import YouTubeIcon from '@material-ui/icons/YouTube';
import FacebookIcon from '@material-ui/icons/Facebook';
import LanguageIcon from '@material-ui/icons/Language';
const useStyles = makeStyles(theme => ({
  root: {
    width:'80%',
    borderRadius: '25px',
    alignItems: 'center',
    
  
    padding: theme.spacing(1),
    marginLeft:theme.spacing(5),
    display: 'flex',
    
  },
  title:{
    height: "34px",
    fontFamily: "Roboto",
    fontStyle: "normal",
    fontWeight: "normal",
    fontSize: "20px",
    lineHeight: "23px",
    display: "flex",
    alignItems: "center",
    marginBottom:"3%",
    color: "#000000",

  },
  icon: {
    marginRight: theme.spacing(1),
    
  },
  input: {
    flexGrow: 1,
    fontSize: '14px',
    lineHeight: '16px',
    letterSpacing: '-0.05px'
  },
  card:{
    marginTop:"5%",
    width:'90%',
    marginLeft:"5%",
  },
  stitle:{
    marginLeft:"5%",
    width: "496px",
    height: "50px",
    fontFamily: "Roboto",
    fontStyle: "normal",
    fontWeight: "normal",
    fontSize: "18px",
    lineHeight: "21px",
    alignItems: "center",
    
    color: "#000000",

  }
}));


const UserList = props => {
  const classes = useStyles();
  const [lien, setLien] = useState("");
  const [lienF, setLienF] = useState("");
  const [lienW, setLienW] = useState("");
  const { className, onChange, style, ...rest } = props;
  const onClick = () =>{
    axios.post(`http://157bb3e084f8.ngrok.io/api/posts/config/sources?source=${lien}`)
   // await axios.get(`https://jsonplaceholder.typicode.com/posts/1`)
    .then(res =>{ 
      
      }
    )
 };
 const onClickF = () =>{
  axios.post(`http://157bb3e084f8.ngrok.io/posts/config/sources?source=${lienF}`)
 // await axios.get(`https://jsonplaceholder.typicode.com/posts/1`)
  .then(res =>{ 
    
    }
  )
};
const onClickW = () =>{
  axios.post(`http://157bb3e084f8.ngrok.io/api/posts/config/sources?source=${lienW}`)
 // await axios.get(`https://jsonplaceholder.typicode.com/posts/1`)
  .then(res =>{ 
    
    }
  )
};
 const handleChange = (event) => {
  setLien(event.target.value)
}
const handleChangeF = (event) => {
  setLienF(event.target.value)
}
const handleChangeW = (event) => {
  setLienW(event.target.value)
}
  return (
    <div>
    <Card  className={classes.card}>
      <CardContent>
      <Typography
      className={classes.title}
      >
  <YouTubeIcon
  fontSize="large"
  style={{ color: "#FF0000"}}
  />
    Paramétres des chaines youtube
</Typography>
<Typography  
 className={classes.stitle}
>
Ajouter une nouvelle chaine
</Typography>
    <Paper
    {...rest}
    className={clsx(classes.root, className)}
    style={style}
  >


    <Input
      {...rest}
      className={classes.input}
      disableUnderline
      id="lien"
      onChange={handleChange}
    />
        <AddCircleIcon
        color="secondary"
        fontSize="large" 
        className={classes.icon} 
        onClick={onClick}
        />
  </Paper>
  </CardContent>
  </Card>
  <Card  className={classes.card}>
      <CardContent>
      <Typography
      className={classes.title}
      >
  <FacebookIcon
  fontSize="large"
  style={{ color: "#4267B2"}}
  />
    Paramétres des pages facebook
</Typography>
<Typography  
 className={classes.stitle}
>
Ajouter une nouvelle page
</Typography>
    <Paper
    {...rest}
    className={clsx(classes.root, className)}
    style={style}
  >


    <Input
      {...rest}
      className={classes.input}
      disableUnderline
      id="lien"
      onChange={handleChangeF}
    />
        <AddCircleIcon
        color="secondary"
        fontSize="large" 
        className={classes.icon} 
        onClick={onClickF}
        />
  </Paper>
  </CardContent>
  </Card>
  <Card  className={classes.card}>
      <CardContent>
      <Typography
      className={classes.title}
      >
  <LanguageIcon
  fontSize="large"
  style={{ color: "#000000"}}
  />
    Paramétres des sites web
</Typography>
<Typography  
 className={classes.stitle}
>
Ajouter un nouveau site
</Typography>
    <Paper
    {...rest}
    className={clsx(classes.root, className)}
    style={style}
  >


    <Input
      {...rest}
      className={classes.input}
      disableUnderline
      id="lien"
      onChange={handleChangeW}
    />
        <AddCircleIcon
        color="secondary"
        fontSize="large" 
        className={classes.icon} 
        onClick={onClickW}
        />
  </Paper>
  </CardContent>
  </Card>
  </div>
  );
};

export default UserList;
