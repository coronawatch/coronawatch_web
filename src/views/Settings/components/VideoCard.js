import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import {
  Card,
  CardContent,
  CardActions,
  Typography,
  Grid,
  Divider
} from '@material-ui/core';
import AccessTimeIcon from '@material-ui/icons/AccessTime';
import GetAppIcon from '@material-ui/icons/GetApp';

const useStyles = makeStyles(theme => ({
  root: {
  height:'100%'
  },
  imageContainer: {
    height: '20%',
    width: '100%',
    margin: '0 auto',
    border: `1px solid ${theme.palette.divider}`,
    borderRadius: '5px',
    overflow: 'hidden',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  image: {
    width: '100%'
  },
  statsItem: {
    display: 'flex',
    alignItems: 'center'
  },
  icon:{
    marginLeft:"60%",
 
  },
  page:{

   
height: "22px",

marginTop:"1%",
fontFamily: "Roboto",
fontStyle: "normal",
fontWeight: "500",
fontSize: "15px",
lineHeight: "15px",
  },
  statsIcon: {
    color: theme.palette.icon,
    marginRight: theme.spacing(1)
  },
  title:{
    
width: '296px',

left:' 305px',
top: '482px',
marginTop:'3%',
marginLeft:'5%',
fontFamily: 'Roboto',
fontStyle:' normal',
fontWeight: 'normal',
fontSize: '24px',
lineHeight: '28px',

alignItems: 'center',
  },
  redacteur:{
   color:"#4EACAC",
   width: '103px',
height: '22px',
marginLeft:'5%',
left: '305px',
top: '519px',

fontFamily: 'Roboto',
fontStyle: 'normal',
fontWeight: '500',
fontSize: '13px',
lineHeight: '15px',

alignItems: 'center',
  },
  date:{
   color:'#C4C4C4',
   width: '103px',
height: '22px',
left: '385px',
top: '519px',
marginLeft:'8%',
fontFamily: 'Roboto',
fontStyle: 'italic',
fontWeight: '300',
fontSize: '11px',
lineHeight: '13px',

alignItems: 'center',

  },
  description:{
  marginTop:'5%',
  marginLeft:'5%',
  },

}));

const VideoCard = props => {
  const { className, product, ...rest } = props;

  const classes = useStyles();
  {console.log(props.product)}
  if ( props.product.detail.type == "FACEBOOK" || props.product.detail.type == "YOUTUBE"){
    console.log(props.product)
  return (
   
    <Card
      {...rest}
      className={clsx(classes.root, className)}
      key={props.product.id}
      >
    
      <CardContent
      className={classes.video}
      dangerouslySetInnerHTML={{__html: props.product.detail.contenu}}
      >
     
      </CardContent>
     
  </Card> )}
  else  {
    console.log(props.product)
    return(
      <Card
      {...rest}
      className={clsx(classes.root, className)}
      key={props.product.id}
      >
    
    
  
    <CardContent
className={classes.video}
    >
      <Grid container>
      <Grid item
        lg={1}
        sm={1}>
      <img src={ props.product.detail.contenu.icon}  className={classes.icon}/> 
      </Grid>
      <Grid item
        lg={11}
        sm={11}>
      <Typography  className={classes.page}>
     { props.product.detail.contenu.siteName}
 </Typography>
 </Grid>
 </Grid>
 <img src={props.product.detail.contenu.cover}/>
 <Typography  className={classes.littit}>
 {props.product.detail.contenu.title}
 </Typography>
      
      
    </CardContent>
   

     
  </Card>)
  }
 
};

VideoCard.propTypes = {
  className: PropTypes.string,
  product: PropTypes.object.isRequired
};

export default VideoCard;
