import React, { useState, useEffect,useContext }  from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { AuthContext , AuthProvider} from '../../../components/Auth';
import {firebase} from "../../Dashboard/components/Firebase/index";
import { makeStyles } from '@material-ui/styles';
import Button from '@material-ui/core/Button';
import axios from 'axios';
import ArrowBackRoundedIcon from '@material-ui/icons/ArrowBackRounded';
import {
  Card,
  CardContent,
  CardActions,
  Typography,
  Grid,
  Divider
} from '@material-ui/core';
import AccessTimeIcon from '@material-ui/icons/AccessTime';
import GetAppIcon from '@material-ui/icons/GetApp';

const useStyles = makeStyles(theme => ({
  root:{
    height:"100%"
  },
    card2: {
  width: '100%',
  height: '80px',
 marginTop:'5%',
  marginBottom:'5%',
  background: '#FFFFFF',
  boxShadow:' 0px 5.5px 5px rgba(0, 0, 0, 0.24), 0px 9px 18px rgba(0, 0, 0, 0.18)',
  borderRadius: '6px',
    },
    delete:{
      position: 'absolute',
     
      left: '290px',
      fontFamily: 'Roboto',
      fontStyle: 'normal',
      fontWeight: 'normal',
      fontSize: '18px',
      lineHeight: '21px',
      color: '#C12020',
     },
     icon:{
      marginLeft:"60%",
   
    },
    page:{
  
     paddingBottom:"2%",
  height: "22px",
  
  marginTop:"1%",
  fontFamily: "Roboto",
  fontStyle: "normal",
  fontWeight: "500",
  fontSize: "15px",
  lineHeight: "15px",
    },
     valide:{
      position: 'absolute',
    
      left: '650px',
      fontFamily: 'Roboto',
      fontStyle: 'normal',
      fontWeight: 'normal',
      fontSize: '18px',
      lineHeight: '21px',
      color: '#4EACAC',
      
     },
   image:{
    position:' absolute',
    width: '25px',
    height: '25px',
    left: '37%',
  },
    image2:{
  position: 'absolute',
  width: '25px',
  height: '25px',
  left: '65%', 
   },
 video2:{
   height:"100%"
 },
 img:{
  
  marginLeft:"45%"
},
href:{
  paddingTop:"5%"
}

  
  
  }));

const VideoNonLu= props=> {
  const { currentUser } = useContext(AuthContext);
  async function token() {
     return await firebase.auth().currentUser.getIdToken()
    
  } 
    const classes = useStyles();
    const { className, ...rest } = props;
    const [showText, setShowText] = React.useState(false)
    const [showImg, setShowImg] = React.useState()
    async function valide  () {
      const auth=  await token()
      console.log(props.id)
        const True="true";
        axios.put(`https://a5e6ffa21bf9.ngrok.io/api/posts/${props.id}?valide=${True}`,{},{ headers: { Authorization: `Bearer ${auth}` }})
        .then(()=>{ 
          setShowText(true);
          setShowImg(true)
          }
        )
     };
     async function deletes ()  {
      console.log(props.Pays)
      const False="false";
      const auth=  await token()
      axios.put(`https://a5e6ffa21bf9.ngrok.io/api/posts/${props.id}?valide=${False}`,{},{ headers: { Authorization: `Bearer ${auth}` }})
        .then(()=>{
          setShowText(true);
          setShowImg(false)
          }
        )
     }
     const falses =()=>{
       props.show(false);
     }
     {console.log(props.postId)}
     if ( props.postId.type != "WEBSITE"){
  
  return (<div>
      <ArrowBackRoundedIcon
    color="#4EE2EC"
    onClick={falses}
    />
<Card
      {...rest}
      className={clsx(classes.root, className)}
     
      >
    <CardContent
       className={classes.video2}
      dangerouslySetInnerHTML={{__html: props.postId.contenu}}
    >
   


    </CardContent>
     
</Card>
{!showText &&  <Card

  className={classes.card2}
>
  <CardContent>
 
  <Button
           
           onClick={deletes}
        variant="contained"
        color="#000000"
        className={classes.button}
        className={classes.delete}
        startIcon={ <img 
           
            src="/images/icons/delete.png"
          
          />}
      >

               ne pas valider l'information
         </Button>


         <Button
           onClick={valide}
        variant="contained"
        color="#000000"
        onClick={valide}
        className={classes.button}
        className={classes.valide}
        startIcon={ <img 
         
          src="/images/icons/valide.png"
          
        />}
      >
        Valider l'information
      </Button>

  </CardContent>
</Card>}
{showText && <Card

className={classes.card2}
>
<CardContent>

{showImg && <img  className={classes.img}  src="/images/icons/valide.png"></img>}
{!showImg && <img  className={classes.img}  src="/images/icons/delete.png"></img>}

</CardContent>
</Card>} 
 </div> )}
 else{ return(
  <div>
  <ArrowBackRoundedIcon
color="#4EE2EC"
onClick={falses}
/>
<Card
      {...rest}
      className={clsx(classes.root, className)}
   
      >
    
    
  
    <CardContent
className={classes.video}
    >
      <Grid container>
      <Grid item
        lg={1}
        sm={1}>
      <img src={props.postId.contenu.icon} className={classes.icon}/> 
      </Grid>
      <Grid item
        lg={11}
        sm={11}>
      <Typography  className={classes.page}>
     {props.postId.contenu.siteName}
 </Typography>
 </Grid>
 </Grid>
 <img src={props.postId.contenu.cover}/>
 <Typography className={classes.page}>
 {props.postId.contenu.title}
 </Typography>
 <Typography>
 <a href={props.postId.contenu.url}
 className={classes.href}
 >
{ props.postId.contenu.description}
 </a>
 </Typography>
    </CardContent>
   

     
  </Card>
{!showText &&  <Card

className={classes.card2}
>
<CardContent>

<Button
       
       onClick={deletes}
    variant="contained"
    color="#000000"
    className={classes.button}
    className={classes.delete}
    startIcon={ <img 
       
        src="/images/icons/delete.png"
      
      />}
  >

           ne pas valider l'information
     </Button>


     <Button
       onClick={valide}
    variant="contained"
    color="#000000"
    onClick={valide}
    className={classes.button}
    className={classes.valide}
    startIcon={ <img 
     
      src="/images/icons/valide.png"
      
    />}
  >
    Valider l'information
  </Button>

</CardContent>
</Card>}
{showText && <Card

className={classes.card2}
>
<CardContent>

{showImg && <img  className={classes.img}  src="/images/icons/valide.png"></img>}
{!showImg && <img  className={classes.img}  src="/images/icons/delete.png"></img>}

</CardContent>
</Card>} 
</div> )
 }

  }
export default VideoNonLu;
