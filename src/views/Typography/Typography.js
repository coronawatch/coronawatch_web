import React,{ useState, useEffect,useContext } from 'react';
import { makeStyles } from '@material-ui/styles';
import styled from 'styled-components'
import Dz from './dz-04.json';
import { VectorMap } from '@south-paw/react-vector-maps'
import axios from 'axios'
import { Grid } from '@material-ui/core';
import {firebase} from "../Dashboard/components/Firebase/index";
import { AuthContext , AuthProvider} from '../../components/Auth';
import {Table} from './components'
import {
  
  Budget,
   Validate,
  TotalUsers,
  TasksProgress,
  TotalProfit,
} from '../Dashboard/components';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(1),
  },
  title:{
    fontFamily: 'Roboto',
    fontStyle: 'normal',
  fontWeight: 'bold',
     fontSize:'24px',
     display: 'flex',
     alignItems: 'center',
   marginTop:'2%',
     marginLeft: '5%',
  
   },

}));


const Typography = () => {
  const [clicked, setClicked] = useState('None');
  const layerProps = {
    onClick: ({ target }) => setClicked(target.attributes.name.value),
  };
  const Map = styled.div`
  margin: 1rem auto;
  

  svg {
    stroke: #fff;
    

    // All layers are just path elements
    path {
      fill: #B6B6B4;
      cursor: pointer;
      outline: none;

      // When a layer is hovered
      &:hover {
        fill: #848482;
      }

      // When a layer is focused.
      &:focus {
        fill: #D1D0CE;
      }

      // When a layer is 'checked' (via checkedLayers prop).
      &[aria-checked='true'] {
        fill: rgba(56,43,168,1);
      }

      // When a layer is 'selected' (via currentLayers prop).
      &[aria-current='true'] {
        fill: rgba(56,43,168,0.83);
      }

      // You can also highlight a specific layer via it's id
      &[id="adrar"] {
        fill:#Alger;
      }
    }
  }
`;
const [count, setCount] = useState([]);
const { currentUser } = useContext(AuthContext);
async function token() {
   return await firebase.auth().currentUser.getIdToken()
  
} 
  const classes = useStyles();
  async function data () {   
    const auth=  await token() 
    console.log(clicked);
   const res = await  axios.get(`/api/carte/national?wilaya=${clicked}&extra`,{  headers: { Authorization: `Bearer ${auth}` }})
     
        setCount(res.data);
        console.log(res);
    
  }
  useEffect(()=>{
  data()
  },[clicked]
  )

  return (
    <div>
     
      <p className={classes.title}>
             {clicked}
      </p>
       <Grid container>
      <Grid item
          lg={9}
          sm={6}>
     <Map>
     <VectorMap {...Dz} 
     layerProps={layerProps}
     />
     </Map>
    </Grid>
    
    <Grid item  lg={3} sm={6}>
    <Grid 
        className={classes.root}
          item
          lg={12}
          sm={12}
          xl={3}
          xs={12}
        >
          <TotalProfit  Pays={clicked} count={count}  />
 
          </Grid>
      <Grid  className={classes.root}
     
          item
          lg={12}
          sm={12}
          xl={3}
          xs={12}
        >
          <TotalUsers   Pays={clicked} count={count} />
          </Grid>
          <Grid className={classes.root}
   
          item
          lg={12}
          sm={12}
          xl={3}
          xs={12}
        >
          <TasksProgress  Pays={clicked} count={count} />
          </Grid>
          
          </Grid>
          <Validate  Pays={clicked} count={count}  nat={"False"}/>
  
      </Grid>
  
      <Table/>

    
    </div>
  );
};

export default Typography;
