import React, { useState, useEffect,useContext} from 'react';
import {firebase} from "../Dashboard/components/Firebase/index";
import { AuthContext , AuthProvider} from '../../components/Auth';
import axios from 'axios'
import CardSign from './components/CardSign';
import { makeStyles } from '@material-ui/styles';
import {
    Card,
    CardContent,
    CardActions,
    Typography,
    Grid,
    Divider
  } from '@material-ui/core';
const CasSuspect= () => {
  const { currentUser } = useContext(AuthContext);
  async function token() {
     return await firebase.auth().currentUser.getIdToken()
    
  } 

const [count, setCount] = useState([]);

async function data(){   
  const auth=  await token() 
  const res = await axios.get(`https://a5e6ffa21bf9.ngrok.io/api/suspects/?status=NON_LU`,{  headers: { Authorization: `Bearer ${auth}` }})
      setCount(res.data);
      console.log(res.data)
}



  return (
    <div >
      {count.map(product => (
           
           <Grid
                item
                key={product.id}
                lg={6}
                md={6}
                xs={12}
              >
               <CardSign  product={product}  id={product.id} />
            
              </Grid>
            )
            
            )}
    
    </div>
  );
};

export default CasSuspect;
