import React, { useState, useEffect,useContext} from 'react';
import {firebase} from "../Dashboard/components/Firebase/index";
import { AuthContext , AuthProvider} from '../../components/Auth';

import axios from 'axios'

import CardVideo from './components/CardVideo';
import { makeStyles } from '@material-ui/styles';
import {
    Card,
    CardContent,
    CardActions,
    Typography,
    Grid,
    Divider
  } from '@material-ui/core';
const VideoIntValide= () => {
  const { currentUser } = useContext(AuthContext);
  async function token() {
     return await firebase.auth().currentUser.getIdToken()
    
  } 

const [count, setCount] = useState([]);

async function data(){  
 
  const auth=  await token() ;
  console.log(auth) ;
  const res = await axios.get(` http://14576360e721.ngrok.io/api/videos/?status=VALIDE`,{  headers: { Authorization: `Bearer ${auth}` }})
      setCount(res.data);
      console.log(res)
}
useEffect(()=>{
data()
},[]
)


  return (
    <div >
      {count.map(product => (
           
           <Grid
                item
                key={product.id}
                lg={6}
                md={6}
                xs={12}
              >
               <CardVideo product={product}  id={product.id} />
            
              </Grid>
            )
            
            )}
    
    </div>
  );
};

export default VideoIntValide;
