import React, { useState, useEffect,useContext } from "react";
import ReactDOM from 'react-dom';
import { AuthContext , AuthProvider} from '../../components/Auth';
import {firebase} from "../Dashboard/components/Firebase/index";
import mapboxgl from 'mapbox-gl';
import { makeStyles } from '@material-ui/styles';
import { Grid } from '@material-ui/core';
import Modal from '@material-ui/core/Modal';
import axios from 'axios'
import {Table} from './components'
mapboxgl.accessToken = 'pk.eyJ1IjoiYWRtaW4taXNsYW0iLCJhIjoiY2s4MGtyd2FnMGhmeDNldW5tYXNlMG54aiJ9.DqCW5md1l6YykOAkokL80A';






const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(1),
  },
  row: {
    width:'80%',
    display: 'flex',
    alignItems: 'center',
    margin: theme.spacing(2)
  },
  
  paper: {
    position: 'absolute',
    backgroundColor: theme.palette.background.paper,
    border: '0 px',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
 title:{
  fontFamily: 'Roboto',
  fontStyle: 'normal',
fontWeight: 'bold',
   fontSize:'24px',
   display: 'flex',
   alignItems: 'center',
 marginTop:'2%',
   marginLeft: '5%',

 }
 ,
 root:{
  margin:" 0", padding: "0",
 },
 absolute:{
   height:"900px",
  width: "100%",
 }
}));

const Zone = () => {
  const { currentUser } = useContext(AuthContext);
async function token() {
   return await firebase.auth().currentUser.getIdToken()
  
} 
const [state, setState] = useState({
 
  lng: 1.8,
  lat: 28.824,
  zoom: 4.8
});
const [longitude,setLongitude]=useState(0)
const [latitude,setLatitude]=useState(0)
const [rayon,setRayon]=useState(0)
const [info,setInfo]=useState([]);
async function data(){  
 
  const auth=  await token() ;
  console.log(auth) ;
  const res = await axios.get(`https://20277f868638.ngrok.io/api/zones/?status=NON_LU`,{  headers: { Authorization: `Bearer ${auth}` }})
     
  setInfo(res.data)
      console.log(res.data)
     
     
}

  
 const   mapRef = React.createRef();
const classes = useStyles();

const [tooltipContainer, setTooltipContainer] = useState()


useEffect(()=>{
  
    const { lng, lat, zoom } = state;
    setTooltipContainer (document.createElement('div'));
  
    const map = new mapboxgl.Map({
      container: mapRef.current,
      style: 'mapbox://styles/admin-islam/ckbpoag7j4h871ilpln3q29ii',
      center: [lng, lat],
      zoom:4.8
    });
    data();
   var marker =new mapboxgl.Marker().setLngLat([2.690405898149777, 35.455512321443194]).addTo(map); 
     
   
   
    map.on('move', () => {
      const { lng, lat } = map.getCenter();

      setState({
        lng: lng.toFixed(4),
        lat: lat.toFixed(4),
        zoom: map.getZoom().toFixed(2)
      });
    });




    map.on('click', (e) => {
      
      
      const features = map.queryRenderedFeatures(e.point);
      
      console.log("f",e.lngLat)
    
      console.log("f",e.lngLat.lat)
      console.log("f",e.lngLat.lng)
      setLatitude(e.lngLat.lat)
      setLongitude(e.lngLat.lng)
    
    });
  
 
}

,[])
const { lng, lat, zoom } = state;
  return (
  
     
        <div className={classes.root}>
          <Grid container>
         
          <div>{`Longitude: ${longitude} Latitude: ${latitude} Zoom: ${zoom}`}</div>
    
        <div  ref={mapRef} className={classes.absolute} />
        </Grid>
        <Grid container
         >
  <Table/>
          </Grid>
        
      
      </div>
    
  );
  
};

export default Zone;
