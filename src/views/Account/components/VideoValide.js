import React, { useState, useEffect,useContext }  from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';

import { makeStyles } from '@material-ui/styles';
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';
import axios from 'axios';
import ArrowBackRoundedIcon from '@material-ui/icons/ArrowBackRounded';
import { AuthContext , AuthProvider} from '../../../components/Auth';
import {firebase} from "../../Dashboard/components/Firebase/index";
import {
  Card,
  CardContent,
  CardActions,
  Typography,
  Grid,
  Divider
} from '@material-ui/core';


const useStyles = makeStyles(theme => ({
  root:{
    height:"80%"
  },
  card:{
      marginTop:"2%"
  },
   name:{
    
    height:" 20px",
    fontFamily: "Roboto",
    fontStyle: "normal",
    fontWeight: "500",
    fontSize: "19px",
    color: "#4EACAC",
   },
   contenu:{
     marginTop:"1%",
    fontFamily: "Roboto",
    fontStyle: "normal",
    fontWeight: "normal",
    fontSize: "12px",
    lineHeight: "14px",
    textAlign: "justify",
    letterSpacing: "0.05em",
    color: "#000000",

   }
  
  
  
  }));

const VideoValide= props=> {
    const classes = useStyles();
    const { className, ...rest } = props;
    const [comment, setComment] = useState([]);
    const { currentUser } = useContext(AuthContext);
    async function token() {
       return await firebase.auth().currentUser.getIdToken()
      
    } 
    useEffect(() => {
      //setData
      async function fetchPosts(){
        const auth=  await token()
        const res= await axios.get(`https://a5e6ffa21bf9.ngrok.io/api/posts/${props.id}/comments`,{ headers: { Authorization: `Bearer ${auth}` }});
     
       setComment(res.data)
  
      
      }
      fetchPosts();
      
     },[]);
     
     async function Delete (id){
      const auth=  await token()
      axios.delete(`https://a5e6ffa21bf9.ngrok.io/api/posts/${props.id}/comments/${id}`,{ headers: { Authorization: `Bearer ${auth}` }})
   
      .then(res =>{ 
        
        }
      )
   };
   const falses =()=>{
    props.show(false);
  }
  if ( props.postId.type !="WEBSITE"){
  return (<div>
        <ArrowBackRoundedIcon
    color="#4EE2EC"
    onClick={falses}
    />
<Card
      {...rest}
      className={clsx(classes.root, className)}
     
      >
    <CardContent>
       <div dangerouslySetInnerHTML={{__html: props.postId.contenu}}></div>
       
        <hr align="center" width="100%" color="#EEEEEE" size="5" />
        <h6>Commentaires</h6>
        <p >{comment.length } commentaires</p>
        {comment.map(cmnt => (
             <Card  className={classes.card}>
             <CardContent>
               <Grid
               container
               direction="row"
               >
               <Grid
                 lg={11}
                 md={11}
                 xs={8}>
             <Typography
              className={classes.name}
             >
               {cmnt.detail.user.name}
             </Typography>
             </Grid>
             <Grid
               lg={1}
               md={1}
               xs={4}>
             <DeleteOutlineIcon
             onClick={()=>Delete(cmnt.id)}
             align="right"
             />
             </Grid>
             </Grid>
             <Typography
              className={classes.contenu}
             >
               {cmnt.detail.contenu}
             </Typography>
             
             </CardContent>
             </Card>
         
            ))}
      

    </CardContent>
     
</Card>
 
 </div> )
}
else{ return(
  <div>
        <ArrowBackRoundedIcon
    color="#4EE2EC"
    onClick={falses}
    />
<Card
      {...rest}
      className={clsx(classes.root, className)}
     
      >
    <CardContent>
   
      <Grid container>
      <Grid item
        lg={1}
        sm={1}>
      <img src={props.postId.contenu.icon} className={classes.icon}/> 
      </Grid>
      <Grid item
        lg={11}
        sm={11}>
      <Typography  className={classes.page}>
     {props.postId.contenu.siteName}
 </Typography>
 </Grid>
 </Grid>
 <img src={props.postId.contenu.cover}/>
 <Typography className={classes.page}>
 {props.postId.contenu.title}
 </Typography>
 <a href={props.postId.contenu.url}>
{ props.postId.contenu.description}
 </a>
  
    
       
        <hr align="center" width="100%" color="#EEEEEE" size="5" />
        <h6>Commentaires</h6>
        <p >{comment.length } commentaires</p>
        {comment.map(cmnt => (
             <Card  className={classes.card}>
             <CardContent>
               <Grid
               container
               direction="row"
               >
               <Grid
                 lg={11}
                 md={11}
                 xs={8}>
             <Typography
              className={classes.name}
             >
               {cmnt.detail.user.name}
             </Typography>
             </Grid>
             <Grid
               lg={1}
               md={1}
               xs={4}>
             <DeleteOutlineIcon
             onClick={()=>Delete(cmnt.id)}
             align="right"
             />
             </Grid>
             </Grid>
             <Typography
              className={classes.contenu}
             >
               {cmnt.detail.contenu}
             </Typography>
             
             </CardContent>
             </Card>
         
            ))}
      

    </CardContent>
     
</Card>
 
 </div> )
}
  }
export default VideoValide;
