import React ,{ useState, useEffect,useContext} from 'react';
import {firebase} from "../../Dashboard/components/Firebase/index";
import { AuthContext , AuthProvider} from '../../../components/Auth';
import PropTypes from 'prop-types';
import "../../../../node_modules/video-react/dist/video-react.css"; 
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import Button from '@material-ui/core/Button';
import axios from 'axios'
import { Player } from 'video-react';
import {
  Card,
  CardContent,
  CardActions,
  Typography,
  Grid,
  Divider
} from '@material-ui/core';
import AccessTimeIcon from '@material-ui/icons/AccessTime';
import GetAppIcon from '@material-ui/icons/GetApp';

const useStyles = makeStyles(theme => ({
  root: {
    height: '100%',
    width: '100%',
    marginTop:"3%",
    marginLeft:"50%"
   
  },
  imageContainer: {
    height: '100%',
    width: '100%',
    margin: '0 auto',
    border: `1px solid ${theme.palette.divider}`,
    borderRadius: '5px',
    overflow: 'hidden',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  image: {
    width: '100%',
    height:"100%"
  },
  statsItem: {
    display: 'flex',
    alignItems: 'center'
  },
  statsIcon: {
    color: theme.palette.icon,
    marginRight: theme.spacing(1)
  },
  title:{
    
width: '296px',

left:' 305px',
top: '482px',
marginTop:'3%',
marginLeft:'5%',
fontFamily: 'Roboto',
fontStyle:' normal',
fontWeight: 'normal',
fontSize: '24px',
lineHeight: '28px',

alignItems: 'center',
  },
  redacteur:{
   color:"#4EACAC",
   width: '103px',
height: '22px',
marginLeft:'5%',
left: '305px',
top: '519px',

fontFamily: 'Roboto',
fontStyle: 'normal',
fontWeight: '500',
fontSize: '13px',
lineHeight: '15px',

alignItems: 'center',
  },
  date:{
   color:'#C4C4C4',
   width: '103px',
height: '22px',
left: '385px',
top: '519px',
marginLeft:'8%',
fontFamily: 'Roboto',
fontStyle: 'italic',
fontWeight: '300',
fontSize: '11px',
lineHeight: '13px',

alignItems: 'center',

  },
  description:{
  marginTop:'5%',
  marginLeft:"5%"
 
  },
  valide:{
      marginLeft:"3%",
      marginTop:"6%"
  },
  img:{
      marginTop:"3%",
      marginLeft:"45%"
  }
}));

const CardVideo = props => {
    const [showText, setShowText] = React.useState(false)
    const [showImg, setShowImg] = React.useState()
    const { currentUser } = useContext(AuthContext);
    async function token() {
       return await firebase.auth().currentUser.getIdToken()
      
    } 
  
    async function valide() {
      const auth=  await token() 
          const True="true";
        const res=  await axios.put(` http://14576360e721.ngrok.io/api/videos/${props.id}?valide=${True}`,{  headers: { Authorization: `Bearer ${auth}` }})
         
    
          .then(()=>{ 
            axios.get(`https://us-central1-coronawatch-62206.cloudfunctions.net/sendMailOverHTTP?dest=ga_fedala@esi.dz&subject=Cas suspect&contenu=un cas a été validé`);
            setShowText(true);
            setShowImg(true)
            }
          )
       };
       async function deletest()  {
        const auth=  await token() 
        const False="false";
        const res = await axios.put(` http://14576360e721.ngrok.io/api/videos/${props.id}?valide=${False}`,{},{  headers: { Authorization: `Bearer ${auth}` }})
    
            setShowText(true);
            setShowImg(false)
           
       }
    
  const { className, product, ...rest } = props;

  const classes = useStyles();

  return (
    <Card
      {...rest}
      className={clsx(classes.root, className)}
      key={props.product.id}
      >
    
      <CardContent>
          <Grid
         item
          >


        <div className={classes.imageContainer}>
       
     
        <Player
      playsInline
      
      src={props.product.detail.src}
    />
        </div>
        <Typography
          align="left"
          gutterBottom
          variant="p"
          className={classes.redacteur}
        >
          {props.product.detail.date}
        </Typography>
        
        <Typography
          align="left"
          variant="body1"
          className={classes.description}
        >
          {props.product.detail.title}
        </Typography>
        </Grid>
        {!showText &&    <Grid
          container
          >
              
     <Button
           
         onClick={deletest}
        variant="contained"
        color="#000000"
        className={classes.button}
        className={classes.valide}
        startIcon={ <img 
           
            src="/images/icons/delete.png"
          
          />}
      >

               Supprimer l'information
         </Button>


         <Button
          onClick={valide}
        variant="contained"
        color="#000000"
     
        className={classes.button}
        className={classes.valide}
        startIcon={ <img 
         
          src="/images/icons/valide.png"
          
        />}
      >
        Valider l'information
      </Button>
        </Grid>}
        {showText && <Grid Container>
        {showImg && <img  className={classes.img}  src="/images/icons/valide.png"></img>}
        {!showImg && <img  className={classes.img}  src="/images/icons/delete.png"></img>}
        </Grid>
        }
      </CardContent>
     
  </Card> )
 
};

CardVideo.propTypes = {
  className: PropTypes.string,
  product: PropTypes.object.isRequired
};

export default CardVideo;
