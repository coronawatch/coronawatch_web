import React, { useState, useEffect,useContext } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import Button from '@material-ui/core/Button';
import axios from 'axios';
import {firebase} from "../../Dashboard/components/Firebase/index";
import { AuthContext , AuthProvider} from '../../../components/Auth';
import ArrowBackRoundedIcon from '@material-ui/icons/ArrowBackRounded';
import {
  Card,
  CardContent,
  CardActions,
  Typography,
  Grid,
  Divider
} from '@material-ui/core';
import AccessTimeIcon from '@material-ui/icons/AccessTime';
import GetAppIcon from '@material-ui/icons/GetApp';

const useStyles = makeStyles(theme => ({
    card2: {
  width: '100%',
  height: '80px',
 marginTop:'5%',
  marginBottom:'5%',
  background: '#FFFFFF',
  boxShadow:' 0px 5.5px 5px rgba(0, 0, 0, 0.24), 0px 9px 18px rgba(0, 0, 0, 0.18)',
  borderRadius: '6px',
    },
   delete:{
    position: 'absolute',
   
    left: '290px',
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: '18px',
    lineHeight: '21px',
    color: '#C12020',
   },
   valide:{
    position: 'absolute',
  
    left: '650px',
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: '18px',
    lineHeight: '21px',
    color: '#4EACAC',
    
   },
   image:{
    position:' absolute',
    width: '25px',
    height: '25px',
    left: '37%',
  },
    image2:{
  position: 'absolute',
  width: '25px',
  height: '25px',
  left: '65%', 
   },
   img:{
  
    marginLeft:"45%"
  }
  
  
  
  }));

const PageNonLu= props=> {
    const classes = useStyles();
    const { className, ...rest } = props;
    const [showText, setShowText] = React.useState(false)
  const [showImg, setShowImg] = React.useState()
  const { currentUser } = useContext(AuthContext);
  async function token() {
    return await firebase.auth().currentUser.getIdToken()
    }
    
   
    async function valide(){
     const auth=  token().then((value)=>{
        const True="true";
      axios.put(`http://157bb3e084f8.ngrok.io/api/articles/${props.id}?valide=${True}`,{},{headers: { Authorization: `Bearer ${value}` }}).then(res=>{
        
          axios.get(`https://us-central1-coronawatch-62206.cloudfunctions.net/notificationCasSuspect?type=article&token=eG0Yf4G0TquZFiWXi1UpVe:APA91bGN1LhTCfOWAxnTMiGeypvSfU8QxePq2tcXUYNFFq_UU8XnSfhQHH0D88MDX5aCXcuMxgH4QS0v_KsO2GlU6ugfoA_tLUbGynaXe0YULZ4gfbwU8HvVkuj400Z47LEDa1C2Bryb&title=sdfsdfsdfsd&body=sdfsfsdfs`);
          setShowText(true);
          setShowImg(true)
        }) 
      })   
     };
     async function deletes () {
      const auth=  await token() 
      const False="false";
      await axios.put(`http://157bb3e084f8.ngrok.io/api/articles/${props.id}?valide=${False}`,{},{  headers: { Authorization: `Bearer ${auth}` } })   
        setShowText(true);
        setShowImg(false)
     }
     const falses =()=>{
      props.show(false);
    }
  
  return (<div>
       <ArrowBackRoundedIcon
    color="#4EE2EC"
    onClick={falses}
    />
<Card
      {...rest}
      className={clsx(classes.root, className)}
     
      >
    <CardContent
    
    dangerouslySetInnerHTML={{__html: props.postId.contenu}}
    >
  

    </CardContent>
     
</Card>
{!showText &&  <Card

  className={classes.card2}
>
  <CardContent>

     <Button
           
           onClick={deletes}
        variant="contained"
        color="#000000"
        className={classes.button}
        className={classes.delete}
        startIcon={ <img 
           
            src="/images/icons/delete.png"
          
          />}
      >

               Supprimer l'information
         </Button>


         <Button
           onClick={valide}
        variant="contained"
        color="#000000"
        onClick={valide}
        className={classes.button}
        className={classes.valide}
        startIcon={ <img 
         
          src="/images/icons/valide.png"
          
        />}
      >
        Valider l'information
      </Button>

  </CardContent>
</Card>}
{showText &&<Card

className={classes.card2}
>
<CardContent>

{showImg && <img  className={classes.img}  src="/images/icons/valide.png"></img>}
        {!showImg && <img  className={classes.img}  src="/images/icons/delete.png"></img>}
</CardContent>
</Card>
}
 </div> )

  }
export default PageNonLu;
