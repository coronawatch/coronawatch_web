import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import {
  Card,
  CardContent,
  CardActions,
  Typography,
  Grid,
  Divider
} from '@material-ui/core';
import AccessTimeIcon from '@material-ui/icons/AccessTime';
import GetAppIcon from '@material-ui/icons/GetApp';

const useStyles = makeStyles(theme => ({
  root: {
   
  },
  imageContainer: {
    height: '20%',
    width: '100%',
    margin: '0 auto',
    border: `1px solid ${theme.palette.divider}`,
    borderRadius: '5px',
    overflow: 'hidden',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  image: {
    width: '100%'
  },
  statsItem: {
    display: 'flex',
    alignItems: 'center'
  },
  statsIcon: {
    color: theme.palette.icon,
    marginRight: theme.spacing(1)
  },
  title:{
    
width: '296px',

left:' 305px',
top: '482px',
marginTop:'3%',
marginLeft:'5%',
fontFamily: 'Roboto',
fontStyle:' normal',
fontWeight: 'normal',
fontSize: '24px',
lineHeight: '28px',

alignItems: 'center',
  },
  redacteur:{
   color:"#4EACAC",
   width: '103px',
height: '22px',
marginLeft:'5%',
left: '305px',
top: '519px',

fontFamily: 'Roboto',
fontStyle: 'normal',
fontWeight: '500',
fontSize: '13px',
lineHeight: '15px',

alignItems: 'center',
  },
  date:{
   color:'#C4C4C4',
   width: '103px',
height: '22px',
left: '385px',
top: '519px',
marginLeft:'8%',
fontFamily: 'Roboto',
fontStyle: 'italic',
fontWeight: '300',
fontSize: '11px',
lineHeight: '13px',

alignItems: 'center',

  },
  description:{
  marginTop:'5%',
  marginLeft:'5%',
  }
}));

const ProductCard = props => {
  const { className, product, ...rest } = props;

  const classes = useStyles();

  return (
    <Card
      {...rest}
      className={clsx(classes.root, className)}
      key={props.product.id}
      >
    
      <CardContent>
        <div className={classes.imageContainer}>
          <img
            alt="cover"
            className={classes.image}
            src={props.product.detail.cover}
          />
        </div>
     
        <Typography
          align="left"
          gutterBottom
          variant="h5"
          className={classes.title}
        >
          {props.product.detail.title}
        </Typography>
        <Typography
          align="left"
          gutterBottom
          variant="p"
          className={classes.redacteur}
        >
          {props.product.detail.author.name}
        </Typography>
        <Typography
          align="left"
          gutterBottom
          variant="p"
          className={classes.date}
        >
          {props.product.detail.date}
        </Typography>
        <Typography
          align="left"
          variant="body1"
          className={classes.description}
        >
          {props.product.detail.description}
        </Typography>
      </CardContent>
     
  </Card> )
 
};

ProductCard.propTypes = {
  className: PropTypes.string,
  product: PropTypes.object.isRequired
};

export default ProductCard;
