import React, { useState, useEffect,useContext } from 'react';
import { makeStyles } from '@material-ui/styles';
import {  Grid, Typography,Card, CardContent ,Link} from '@material-ui/core';
import {  ProductCard } from './components';
import Pagination from './components/Pagination';
import PageNonLu from './components/PostNonLu';
import axios from 'axios';
import {firebase} from "../Dashboard/components/Firebase/index";
import { PieChart } from 'react-minimal-pie-chart';
import { CircularProgressbar, buildStyles } from 'react-circular-progressbar';
import { AuthContext , AuthProvider} from '../../components/Auth';
import 'react-circular-progressbar/dist/styles.css';
const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3)
  },
  content: {
    marginTop: theme.spacing(2)
  },
  pagination: {
    marginTop: theme.spacing(3),
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end'
  },
  publication :{
    
width: '316px',
height: '472px',
left: '940px',
marginLeft:'5%',

background: '#F2F2F2',
borderRadius: '6px',
  },
  titlepub:{

height: '54px',
left: '953px',
top: '192px',
fontFamily: 'Roboto',
fontStyle: 'normal',
fontWeight: '500',
fontSize: '24px',
lineHeight: '28px',
display:' flex',
alignItems: 'center',
marginLeft:'3%',
color: '#000000',

  },
  cardpub:{
    
    marginLeft:'4%',
    width: '289px',
height: '80%',
paddingTop:'10%'
  },
cardtitle:{
  fontFamily: 'Roboto',
fontStyle: 'normal',
fontWeight:' 500',
fontSize: '14px',
lineHeight: '16px',
letterSpacing:' 0.05em',
marginBottom:'3%',
}
}));

const ProductList = () => {
  const classes = useStyles();

  
  const pageLimit = 6;
  const [showText, setShowText] = React.useState(false)
 
  const [currentPage, setCurrentPage] = useState(1);
  const [postId, setPostId] = useState([]);
  const [id, setId] = useState('');
  const [data, setData] = useState([]);//posts
  const [data1, setData1] = useState([]);//posts
  const [data2, setData2] = useState([]);//posts
const { currentUser } = useContext(AuthContext);
  async function token() {
     return await firebase.auth().currentUser.getIdToken()
    
  } 
  async function fetchPosts(){
    const auth=  await token() 
      
     const res=  await axios.get('http://157bb3e084f8.ngrok.io/api/articles/?status=NON_LU',{  headers: { Authorization: `Bearer ${auth}` }})  

     const res2= await  axios.get('http://157bb3e084f8.ngrok.io/api/articles/?status=VALIDE',{   headers: { Authorization: `Bearer ${auth}` }}) 
     const res1= await  axios.get('http://157bb3e084f8.ngrok.io/api/articles/?status=INVALIDE',{  headers: { Authorization: `Bearer ${auth}` }}) 
     setData(res.data);
     setData1(res1.data);
      setData2(res2.data);
    
     
    
    
  }
  useEffect(() => {
   //setData
   fetchPosts();
   
  },[]);
  async function ShowArticle (id){
  setId(id)
  console.log(id)
    setShowText(true);
    const auth=  await token() 
   const res= await axios.get(`http://157bb3e084f8.ngrok.io/api/articles/${id}?extra`,{ headers: { Authorization: `Bearer ${auth}` }})
    
      setPostId(res.data);
    
    
 };
 
const percentage = 66;
  /*useEffect(() => {
    
    //setCurrentData (data.slice(offset,offset+pageLimit))
  }, [offset, data]);*/
 const currentPosts=data.slice(currentPage*pageLimit-pageLimit,currentPage*pageLimit)
 const paginate= (pageNumber) => setCurrentPage(pageNumber)
  return (
    <div className={classes.root}>
      <Typography
                  className={classes.title}
                  variant="h3"
                >
                  Récents
       </Typography>
      <div className={classes.content}>
      <Grid
      container
      direction="row"
      
      >
      
     { !showText &&   <Grid
          container
          spacing={3}
          lg={8}
          md={6}
          xs={12}
        >
           
          {currentPosts.map(product => (
           
         <Grid
              item
              key={product.id}
              lg={6}
              md={6}
              xs={12}
            >
             <ProductCard   product={product} onClick={()=>ShowArticle(product.id)} />
          
            </Grid>
          )
          
          )}
           <Grid container>
      <Pagination  postsPerPage={pageLimit} totalPosts={data.length} paginate={paginate}/>
      </Grid>
 </Grid>
 
        }
      
        
        
    
     
        {showText &&   <Grid
          container
          spacing={3}
          lg={8}
          md={6}
          xs={12}
        >
         <Grid
              item
           
              lg={12}
              md={12}
              xs={12}
            >
        <PageNonLu postId={postId} id={id} show={setShowText}/>
       
        </Grid>
         </Grid>
        }
        <Grid
          container
          lg={4}
          md={6}
          xs={12}
          >
        <Grid
        container
        lg={12}
        md={6}
        xs={12}
        className={classes.publication}
        >
        <Typography
         className={classes.titlepub}>
          Statistiques
        </Typography>
        <Card  className={classes.cardpub}>
          <CardContent>
          <CircularProgressbar
  value={((data.length/(data.length+data1.length+data2.length))*100)}
  text={`${((data.length/(data.length+data1.length+data2.length))*100).toFixed(1)}%`}
  styles={buildStyles({
    // Rotation of path and trail, in number of turns (0-1)
    rotation: 0.25,
 
    // Whether to use rounded or flat corners on the ends - can use 'butt' or 'round'
    strokeLinecap: 'round',
 
    // Text size
    textSize: '14px',
 
    // How long animation takes to go from one percentage to another, in seconds
    pathTransitionDuration: 0.5,
 
    // Can specify path transition in more detail, or remove it entirely
    // pathTransition: 'none',
 
    // Colors
    pathColor: `rgba(62, 152, 199, ${((data.length/(data.length+data1.length+data2.length))*100) / 100})`,
    textColor: '#0B0B3B',
    trailColor: '#d6d6d6',
    backgroundColor: '#3e98c7',
  })}
/>
          </CardContent>
          </Card>
          
         
         
          </Grid>
         
          </Grid>
          </Grid>
      </div>
    
    </div>
  );
};

export default ProductList;
